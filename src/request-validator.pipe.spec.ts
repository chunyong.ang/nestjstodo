import { RequestValidatorPipe } from './request-validator.pipe';

describe('RequestValidatorPipe', () => {
  it('should be defined', () => {
    expect(new RequestValidatorPipe()).toBeDefined();
  });
});
