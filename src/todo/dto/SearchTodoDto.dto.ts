import { IsString, IsBoolean } from 'class-validator';

export class SearchTodoDto{
    @IsString()
    title?:string;

    @IsBoolean()
    isActive?:boolean;
}