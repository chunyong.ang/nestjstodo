import { IsString, IsBoolean, IsNotEmpty } from 'class-validator';

export class CreateTodoDto{
    @IsString()
    @IsNotEmpty()
    title:string;

    @IsString()
    description?:string;

    @IsBoolean()
    isActive?:boolean;
}