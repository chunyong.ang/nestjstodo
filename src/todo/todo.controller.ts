import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post } from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/CreateTodoDto.dto';
import { SearchTodoDto } from './dto/SearchTodoDto.dto';
import { Todo } from './schema/todo.schema';
import { RequestValidatorPipe } from './../request-validator.pipe';


@Controller('todo')
export class TodoController {
    private mongoose = require('mongoose');
    constructor(private todoService: TodoService) {}
    
    @Get()
    async findAll(): Promise<Todo[]>{
        return this.todoService.findAll();
    }

    @Get(':id')
    async getDetail(@Param('id') id): Promise<Todo>{
        //Validate mongo id
        if (!this.mongoose.Types.ObjectId.isValid(id)){
            throw new HttpException("Invalid request", HttpStatus.BAD_REQUEST);
        }
        return this.todoService.findDetail(id);
    }

    @Post()
    async createTodo(@Body(new RequestValidatorPipe()) createTodoDto: CreateTodoDto): Promise<Todo>{
        return this.todoService.create(createTodoDto);
    }

    @Post('search')
    async searchTodo(@Body() SearchTodoDto: SearchTodoDto): Promise<Todo[]>{
        return this.todoService.findSearch(SearchTodoDto);
    }

    @Patch(':id')
    async updateTodo(@Param('id') id, @Body(new RequestValidatorPipe()) createTodoDto: CreateTodoDto): Promise<Todo>{
        //Validate mongo id
        if (!this.mongoose.Types.ObjectId.isValid(id)){
            throw new HttpException("Invalid request", HttpStatus.BAD_REQUEST);
        }
        return this.todoService.update(id, createTodoDto);
    }

    @Delete(':id')
    async deleteTodo(@Param('id') id): Promise<Todo>{
        //Validate mongo id
        if (!this.mongoose.Types.ObjectId.isValid(id)){
            throw new HttpException("Invalid request", HttpStatus.BAD_REQUEST);
        }
        return this.todoService.delete(id);
    }
}
