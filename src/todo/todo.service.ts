import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Todo, TodoDoc } from './schema/todo.schema';
import { CreateTodoDto } from './dto/CreateTodoDto.dto';
import { SearchTodoDto } from './dto/SearchTodoDto.dto';

@Injectable()
export class TodoService {
    constructor(@InjectModel("todos") private TodoModel: Model<TodoDoc>){}

    async findAll(): Promise<Todo[]>{
        return this.TodoModel.find();
    }

    async findSearch(SearchTodoDto: SearchTodoDto): Promise<Todo[]>{
        let find = {};
        for (let k in SearchTodoDto){
            if (SearchTodoDto[k] !== ""){
                find[k] = SearchTodoDto[k];
            }
        }
        return this.TodoModel.find(find);
    }

    async findDetail(id:string): Promise<Todo>{
        return this.TodoModel.findById(id);
    }

    async create(CreateTodoDto: CreateTodoDto): Promise<Todo>{
        const ctodo = new this.TodoModel(CreateTodoDto);
        return ctodo.save();
    }

    async update(id, CreateTodoDto: CreateTodoDto): Promise<Todo>{
        return this.TodoModel.findByIdAndUpdate(id, CreateTodoDto, {new: true});
    }

    async delete(id): Promise<Todo>{
        return this.TodoModel.findByIdAndRemove(id);
    }
}
