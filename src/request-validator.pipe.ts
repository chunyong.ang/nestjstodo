import { ArgumentMetadata, Injectable, PipeTransform, HttpException, HttpStatus } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';

@Injectable()
export class RequestValidatorPipe implements PipeTransform<any> {
  async transform(value: any, {metatype }: ArgumentMetadata) {
    const object = plainToInstance(metatype , value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new HttpException('Invalid request', HttpStatus.BAD_REQUEST);
    }
    return value;
  }

  private toValidate(metadata: Function): boolean {
    const types: Function[] = [String, Boolean, Number];
    return !types.includes(metadata);
  }
}
